#!/bin/sh

basedir=$(pwd)
pkgsdir=$basedir/pkgs
rpmdir=$pkgsdir/rpms
debdir=$pkgsdir/debs
rpmarchives="rpm.list"
debarchives="deb.list"

info_log() {
	if [ -n "$color" ]; then
		printf "\033[34m[tinybuild]\033[0m info: $1\n"
	else
		printf "[tinybuild] info: $1\n"
	fi
}

warn_log() {
	if [ -n "$color" ]; then
		printf "\033[34m[tinybuild]\033[33m warn\033[0m: $1\n"
	else
		printf "[tinybuild] warn: $1\n"
	fi
}

fetch_archives() {
	# fetch and move package
	fnmp() {
			info_log "downloading $1 package"
			info_log "downloading $2"
			curl -Ls -o "$2" "$3"
			info_log "moving $1 package"
			mv "$2" "$4"
	}

	while read -r u; do
		_name=$(echo "$u" | cut -d'|' -f1)
		_type=$(echo "$u" | cut -d'|' -f2)
		_url=$(echo "$u" | cut -d'|' -f3)
		case $_type in
			tbd)
				info_log "downloading tinybuild out archive"
				info_log "downloading $_name"
				curl -Ls -o "$_name" "$_url"
				info_log "unpacking $_name"
				unzip "$_name"
				;;
			deb)
				fnmp "deb" "$_name" "$_url" "$basedir/.tinybuild/out/debs"
				;;
			rpm)
				fnmp "rpm" "$_name" "$_url" "$basedir/.tinybuild/out/rpms"
				;;
			*)
				warn_log "unknown archive type specified ($_type)"
				;;
		esac
	done < "$1"
}

prep_keys() {
	info_log "preparing keys"

	openssl aes-256-cbc -d -in keys.tar.enc -out keys.tar -k $enc_key
	tar xf keys.tar

	gpg2 --import keys/pub.gpg
	gpg2 --import keys/priv.gpg
}

make_rpm() {
	info_log "make rpm dir"

	mkdir -p "$rpmdir"

	info_log "fetching rpm archives"

	fetch_archives "$rpmarchives"

	info_log "copying rpms"
	cp -r .tinybuild/out/rpms/* "$rpmdir"

	info_log "sign rpm packages"
	rpm --define "%_gpg_name $repo_owner_name <$repo_owner_email>" --addsign "$rpmdir"/*.rpm

	info_log "create rpm repo"
	createrepo_c --database --compatibility "$rpmdir"

	info_log "sign rpm repo"
	gpg2 --local-user "$repo_owner_name <$repo_owner_email>" --yes --detach-sign --armor "$rpmdir/repodata/repomd.xml"
}

make_deb() {
	info_log "make deb config dir"
	mkdir -p "$debdir/conf"
	cp "$basedir/reprepro_conf"/* "$debdir/conf"

	info_log "make deb dir"
	mkdir -p "$debdir"

	info_log "fetching deb archives"
	fetch_archives "$debarchives"

	info_log "create deb repo"
	reprepro -V -b "$debdir" includedeb dusan .tinybuild/out/debs/*.deb
}

info_log "begin making repos"

prep_keys

[ -f "$rpmarchives" ] && make_rpm
[ -f "$debarchives" ] && make_deb

info_log "copy public key to packages dir"
cp keys/pub.gpg $pkgsdir/key.gpg
